#### Algorithm
- Application processes all data files that were provided as arguments.
    - Data file is parsed to *SortedSet* type to get more precise idea, what the data file is about.
    - If the set of data from file is composed,
        - *SatellitesFactory* creates *List* of *Satellite* type objects with their start and end times.
        - Search for satellites that have overlapping times begins. This composes a list with overlaps for each satellite. *(As long as data in file(s) is provided in 'LocalTime' format, there is no need to convert 'SortedSet' of strings to 'LocalTime' type set)*
        - Then search for a maximum time overlaps begins. This composes a list of maximum time overlaps.
        - If the list is not empty
            - Indices of overlaps are searched, where maximum time overlaps occur.
            - Then every list of maximum overlaps, which corresponds to found index, is separated by start and end times to *SortedSet*s.
                - The earliest start time and the latest end time is printed out alongside with maximum time overlaps - number of satellites - in format: '\<start time\>-\<end time\>;\<number of satellites\>'
        - Else next file is being processed.
    - Else next file is being processed.

Explanation for solving the problem in this way is depicted in '**Other possible solutions**' section.

#### Forseen limitations
- *LocalTime* type is constrained to 24-hour day. It means that it cannot recognize a time, whether it is over midnight or not. Only 1 day data can be fed to the application to give out intended result.
- Satellites with absolute same start and end times will be counted as 1 satellite. According to the time precision of start and end times per satellite such chance is very low. However it is possible (that 1-2 satellites won't be counted per 1001 units).

#### Other possible solutions
- The initial idea was to sort by ascending order start and end times of satellites. The idea did not entail further development of solution due to realization, that start time did not mach end time of specific satellite anymore. Therefore there is *parseLinesToStartAndEndTimesSortedSets* method in *Parser* class.
- I suspect that there is no need to sort out by ascending order times of satellites in order to achieve desired result. Though I decided to spend time on learning about *Collection* types and their possibilities, because I have not had an opportunity to work with them a lot. This decision helps to read easily provided data. *(Solution without sorting awaits to be checked just for interest)*
    - I presume there are other *Collection* types that can fit better to solve the assignment. I faced with the fact that *Set* interface cannot store duplicate items like *List* does. However, due to sorting capability of *Set* I decided to continue with it, what led me to implement solution of storing duplicate items, if such there are. Perhaps *Map* interface could also do the job. But I could not find decent solution for sorting and keeping satellites' data with *Map* *(room for learning and improvement)*.