package com.home;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.SortedSet;
import java.util.TreeSet;

// Candidate for removal
public class SetConverter {

    public SortedSet<LocalTime> convertStringSetToLocalTimeSet(SortedSet<String> set) {
        SortedSet<LocalTime> localTimeSortedSet = new TreeSet<>();

        try {
            for (Object timeString : set) {
                localTimeSortedSet.add(parseTimeStringFormatToLocalTimeFormat(timeString.toString()));
            }
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }

        return localTimeSortedSet;
    }

    private LocalTime parseTimeStringFormatToLocalTimeFormat(String time) throws DateTimeParseException {
        return LocalTime.parse(time);
    }

}
