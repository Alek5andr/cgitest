package com.home;

import com.Entry.Satellite;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.SortedSet;

import static com.home.Main.helper;

public class SatellitesFactory {
    private LinkedList<Satellite> listOfSatellites = new LinkedList<>();

    public SatellitesFactory(SortedSet<String> startAndEndTimesSet) {
        startAndEndTimesSet.forEach(startAndEndTimes -> {
            String[] timesOfSatellite = splitStartAndEndTimesOfSatellite(startAndEndTimes);

            try {
                if (timesOfSatellite.length == 2) {
                    this.listOfSatellites.add(new Satellite(LocalTime.parse(timesOfSatellite[0]), LocalTime.parse(timesOfSatellite[1])));
                } else {
                    throw new IllegalArgumentException("Parsing >2< data entries of a satellite is not implemented yet: " + startAndEndTimes);
                }
            } catch (DateTimeParseException exception) {
                exception.printStackTrace();
            }
        });
    }

    public LinkedList<Satellite> createListOfSatellites(SortedSet<String> startAndEndTimesSet) {
        LinkedList<Satellite> listOfSatellites = new LinkedList<>();

        startAndEndTimesSet.forEach(startAndEndTimes -> {
            String[] timesOfSatellite = splitStartAndEndTimesOfSatellite(startAndEndTimes);

            if (timesOfSatellite.length == 2) {
                listOfSatellites.add(new Satellite(LocalTime.parse(timesOfSatellite[0]), LocalTime.parse(timesOfSatellite[1])));
            } else {
                throw new IllegalArgumentException("Parsing >2< data entries of a satellite is not implemented yet: " + startAndEndTimes);
            }
        });

        return listOfSatellites;
    }

    private String[] splitStartAndEndTimesOfSatellite(String satellite) {
        return helper.splitTextToStringsByRegEx(satellite, ",");
    }

    public LinkedList<Satellite> getListOfSatellites() {
        return listOfSatellites;
    }
}
