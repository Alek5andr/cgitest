package com.home;

public class Helper {

    public String[] splitTextToStringsByRegEx(String text, String regEx) {
        if (text.contains(regEx)) return text.split(regEx);
        throw new IllegalArgumentException("Text does not contain '" + regEx + "': " + text);
    }
}
