
package com.home;

import com.Entry.Satellite;

import java.time.LocalTime;
import java.util.*;

public class Sorter {
    private SortedSet<LocalTime> startTimes = null;
    private SortedSet<LocalTime> endTimes = null;

    public SortedSet<LocalTime> getStartTimes() {
        return startTimes;
    }

    public SortedSet<LocalTime> getEndTimes() {
        return endTimes;
    }

    public LinkedList<ArrayList<Satellite>> findOverlappingSatellites(LinkedList<Satellite> satellites) {
        LinkedList<ArrayList<Satellite>> listOfOverlappingSatellites = new LinkedList<>();

        if (satellites != null) {
            for (Satellite sortedSatellite : satellites) {
                ArrayList<Satellite> overlappingSatellites = new ArrayList<>();

                overlappingSatellites.add(sortedSatellite);

                for (Satellite satellite : satellites) {
                    boolean isOverlap = false;

                    if (sortedSatellite.getStartTime() != satellite.getStartTime()
                            && sortedSatellite.getEndTime() != satellite.getEndTime()) isOverlap = isOverlapping
                            (sortedSatellite.getStartTime(), sortedSatellite.getEndTime(),
                                    satellite.getStartTime(), satellite.getEndTime());

                    if (isOverlap) {
                        overlappingSatellites.add(satellite);
                    }
                }
                listOfOverlappingSatellites.add(overlappingSatellites);
            }
        }

        return listOfOverlappingSatellites;
    }

    public LinkedList<Integer> findIndicesOfMaximumOverlappingInSatellitesList(LinkedList<ArrayList<Satellite>> listOfOverlappingSatellites, int numberOfMaxOverlappings) {
        LinkedList<Integer> indicesOfMaximumOverlapping = new LinkedList<>();

        listOfOverlappingSatellites.forEach(overlappingSatellites -> {
            if (overlappingSatellites.size() == numberOfMaxOverlappings) {
                indicesOfMaximumOverlapping.add(listOfOverlappingSatellites.indexOf(overlappingSatellites));
            }
        });

        return indicesOfMaximumOverlapping;
    }

    public ArrayList<Satellite> getMaxOverlappings(LinkedList<ArrayList<Satellite>> listOfOverlappingSatellites) {
        return listOfOverlappingSatellites.isEmpty() ? null : Collections.max(listOfOverlappingSatellites, Comparator.comparing(ArrayList::size));
    }

    public void separateStartAndEndTimesOfOverlappingsToSortedSets(ArrayList<Satellite> satelliteWithMaxOverlapping) {
        this.startTimes = new TreeSet<>();
        this.endTimes = new TreeSet<>();

        satelliteWithMaxOverlapping.forEach(satellite -> {
            this.startTimes.add(satellite.getStartTime());
            this.endTimes.add(satellite.getEndTime());
        });
    }

    private boolean isOverlapping(LocalTime startTime1, LocalTime endTime1, LocalTime startTime2, LocalTime endTime2) {
        return !startTime1.isAfter(endTime2) && !startTime2.isAfter(endTime1);
    }
}
