package com.home;

import com.Entry.Satellite;

import java.util.*;

public class Main {
    public static Helper helper = new Helper();

    public static void main(String[] args) {
        Sorter sorter = new Sorter();

        for (String arg : args) {
            Parser parser = new Parser(arg);

            parser.parseLinesToStartAndEndTimesSortedSet();

            SortedSet<String> startAndEndTimesSet = parser.getStartAndEndTimesSet();

            if (startAndEndTimesSet != null && !startAndEndTimesSet.isEmpty()) {
                SatellitesFactory satellitesFactory = new SatellitesFactory(startAndEndTimesSet);
                LinkedList<ArrayList<Satellite>> listOfOverlappingSatellites = sorter.findOverlappingSatellites(satellitesFactory.getListOfSatellites());

                ArrayList<Satellite> listOfMaxOverlaps = sorter.getMaxOverlappings(listOfOverlappingSatellites);
                if (listOfMaxOverlaps != null) {
                    List<Integer> indices = sorter.findIndicesOfMaximumOverlappingInSatellitesList(listOfOverlappingSatellites, listOfMaxOverlaps.size());

                    indices.forEach(index -> {
                        ArrayList<Satellite> maxOverlaps = listOfOverlappingSatellites.get(index);
                        sorter.separateStartAndEndTimesOfOverlappingsToSortedSets(maxOverlaps);
                        System.out.println(sorter.getStartTimes().first() + "-" + sorter.getEndTimes().last() + ";" + maxOverlaps.size());
                    });
                }
            }
        }
    }
}
