package com.home;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static com.home.Main.helper;

public class Parser {
    private String filePath;
    private SortedSet<String> stringStartTimes = new TreeSet<>();
    private SortedSet<String> stringEndTimes = new TreeSet<>();
    private SortedSet<String> startAndEndTimesSet = new TreeSet<>();
    private int numberOfSatellites = 0;

    public Parser(String filePath) {
        this.filePath = filePath;
    }

    public void parseLinesToStartAndEndTimesSortedSet() {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(satellite -> {
                this.startAndEndTimesSet.add(satellite);
                numberOfSatellites++;
            });
        } catch (IOException e) {
            System.out.printf("No such file found - %s\n", filePath);
            e.printStackTrace();
        }
    }

    // Candidate for removal
    public void parseLinesToStartAndEndTimesSortedSets() {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(satellite -> {
                String[] timesOfSatellite = helper.splitTextToStringsByRegEx(satellite, ",");
                if (timesOfSatellite.length == 2) {
                    stringStartTimes.add(timesOfSatellite[0]);
                    stringEndTimes.add(timesOfSatellite[1]);
                } else {
                    throw new IllegalArgumentException("Parsing >2< data entries of a satellite is not implemented yet: " + satellite);
                }
            });
        } catch (IOException e) {
            System.out.printf("No such file found - %s\n", filePath);
            e.printStackTrace();
        }
    }

    public SortedSet<String> getStringStartTimes() {
        return stringStartTimes;
    }

    public SortedSet<String> getStringEndTimes() {
        return stringEndTimes;
    }

    public SortedSet<String> getStartAndEndTimesSet() {
        return startAndEndTimesSet;
    }

    public int getNumberOfSatellites() {
        return numberOfSatellites;
    }
}
