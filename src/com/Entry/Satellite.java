package com.Entry;

import java.time.LocalTime;

public class Satellite {
    private LocalTime startTime = null;
    private LocalTime endTime = null;

    public Satellite(LocalTime startTime, LocalTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }
}
